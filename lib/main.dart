import 'dart:convert';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget myimage = Container(
        padding: const EdgeInsets.all(40),
        child: Image.asset(
          'images/sirdech.jpg',
          width: 384.5,
          height: 512,
        ));

    Widget textName = Container(
        child: Column(children: [
      Text('THANAWUT BOONTANAWONG',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w900,
            fontSize: 24,
          ))
    ]));

    Widget positionBar = Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(children: [
          Container(
              color: Colors.black,
              height: 60,
              width: 1600,
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Row(children: [
                Icon(
                  Icons.favorite,
                  color: Colors.white,
                  size: 25,
                ),
                Padding(padding: const EdgeInsets.fromLTRB(0, 0, 10, 0)),
                Text('POSITION SOUGHT',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                    ))
              ]))
        ]));

    Widget positionText = Container(
        padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
        child: Text('WEB DESIGNER',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.w600,
              fontSize: 16,
            )));

    Widget personalBar = Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(children: [
          Container(
              color: Colors.black,
              height: 60,
              width: 1600,
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Row(children: [
                Icon(
                  Icons.account_circle,
                  color: Colors.white,
                  size: 25,
                ),
                Padding(padding: const EdgeInsets.fromLTRB(0, 0, 10, 0)),
                Text('PERSONAL INFORMATION',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                    ))
              ]))
        ]));

    Widget personalText = Container(
        padding: const EdgeInsets.fromLTRB(40, 0, 40, 0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  width: 200,
                  child: Text('GENDER:',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ))),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('MALE',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w900,
                            fontSize: 16,
                          ))
                    ]),
              )
            ],
          ),
          Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 10)),
          Row(
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  width: 200,
                  child: Text('DATE OF BIRTH:',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ))),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('JULY 15, 1999',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w900,
                            fontSize: 16,
                          ))
                    ]),
              )
            ],
          ),
          Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 10)),
          Row(
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  width: 200,
                  child: Text('MARITAL STATUS:',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ))),
              Container(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('SINGLE',
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w900,
                            fontSize: 16,
                          ))
                    ]),
              )
            ],
          ),
        ]));

    Widget educationBar = Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(children: [
          Container(
              color: Colors.black,
              height: 60,
              width: 1600,
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Row(children: [
                Icon(
                  Icons.school_rounded,
                  color: Colors.white,
                  size: 25,
                ),
                Padding(padding: const EdgeInsets.fromLTRB(0, 0, 10, 0)),
                Text('EDUCATION',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                    ))
              ]))
        ]));

    Widget educationText = Container(
        padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
        child: Column(children: [
          Row(
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  width: 200,
                  child: Text('2018 - PRESENT',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ))),
              Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    Text(
                        'B.Sc. (Computer Science) – in progress Faculty of Infortatics Burapha University 169 Longhad Rd., Mueang, Chon Buri 20131',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w900,
                          fontSize: 16,
                        ))
                  ]))
            ],
          ),
          Padding(padding: const EdgeInsets.fromLTRB(0, 0, 0, 10)),
          Row(
            children: [
              Container(
                  margin: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                  width: 200,
                  child: Text('2016 – 2013',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w900,
                        fontSize: 16,
                      ))),
              Container(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    Text(
                        'High School Certificate Paknumchumphon Wittaya School Mueang, Chumphon 86120',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w900,
                          fontSize: 16,
                        ))
                  ]))
            ],
          )
        ]));

    Widget skillsBar = Container(
        margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
        child: Column(children: [
          Container(
              color: Colors.black,
              height: 60,
              width: 1600,
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Row(children: [
                Icon(
                  Icons.military_tech_rounded,
                  color: Colors.white,
                  size: 25,
                ),
                Padding(padding: const EdgeInsets.fromLTRB(0, 0, 10, 0)),
                Text('SKILLS',
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w900,
                      fontSize: 20,
                    ))
              ]))
        ]));

    Widget skillsSection = Container(
        child: Column(children: [
      Row(children: [
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/html.png',
              width: 40,
              height: 40,
            )),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/css.png',
              width: 40,
              height: 40,
            )),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/js.png',
              width: 40,
              height: 40,
            )),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/vue.png',
              width: 40,
              height: 40,
            ))
      ]),
      Row(children: [
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/flutter.png',
              width: 40,
              height: 40,
            )),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/vscode.png',
              width: 40,
              height: 40,
            )),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/netbeans.png',
              width: 40,
              height: 40,
            )),
        Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 20),
            child: Image.asset(
              'images/java.png',
              width: 40,
              height: 40,
            ))
      ])
    ]));

    return MaterialApp(
        title: 'resume layout demo',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('MY RESUME'),
          ),
          body: ListView(children: [
            myimage,
            textName,
            positionBar,
            positionText,
            personalBar,
            personalText,
            educationBar,
            educationText,
            skillsBar,
            skillsSection
          ]),
        ));
  }
}
